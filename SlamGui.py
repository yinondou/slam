import pygame
from SlamEnvironment import SlamEnvironment

class SlamGuiTest:

    BLACK = (0,0,0);
    WHITE = (255,255,255);
    CYAN = (0,255,255);
    YELLOW = (255,255,0);
    GREEN = (0,255,0);
    GREY = (128,128,128);
    BLUE = (0, 0, 255)

    def __init__(self, environment):
        self.environment = environment
        self.grid_handles = [[None for i in enumerate(environment.grid)] for j in enumerate(environment.grid[0])]
        #self.draw_outline()
        self.draw_all()

    def draw_all(self):
        grid = self.environment.grid
        grid_width = len(grid[0])
        grid_height = len(grid)

        square_size = int(0.9*800/max(grid_width, grid_height))
        top_offset = 10
        left_offset = 10
        margin = 1;

        # draw the grid
        for i in range(len(grid)):
            fill = (0,0,0);
            for j in range(len(grid[i])):
                x_pos = square_size * j + left_offset
                y_pos = square_size * i + top_offset

                mapped = False
                los = False
                border = False

                if grid[i][j] == 0: # non-wall
                    fill = (128, 128, 255)
                if self.environment.agent.mapped_area[j][i] == 1: # mapped area
                    fill = self.YELLOW
                    mapped = True
                if self.environment.agent.mapped_area_borders[j][i] == 1: # border of mapped area
                    belief = self.environment.agent.find_border(j, i)[2]
                    fill = (255*(1-belief), 255*(1-belief), 255)
                if self.environment.agent.in_line_of_sight(j, i): # line of sight
                    fill = self.GREY
                    los = True
                if grid[i][j] == 1:
                    fill = self.BLACK

                self.grid_handles[i][j] = pygame.draw.rect(screen, fill, pygame.Rect(x_pos, y_pos, square_size-margin, square_size-margin), 0)

                # draw on walls also as X
                if grid[i][j] == 1:
                    radius = (square_size - margin)/2
                    circle_radius = (square_size - margin)/3
                    if los: # wall on line of sight
                        pygame.draw.circle(screen, self.GREY, (x_pos+radius, y_pos+radius), circle_radius)
                    elif border: # wall on border
                        belief = self.environment.agent.find_border(j, i)[2]
                        fill = (200*(1-belief), 200*(1-belief), 255);
                        pygame.draw.circle(screen, fill, (x_pos+radius, y_pos+radius), circle_radius)
                    elif mapped: # wall on mapped area
                        pygame.draw.circle(screen, self.YELLOW, (x_pos+radius, y_pos+radius), circle_radius)

                # add information gain on borders
                if self.environment.agent.mapped_area_borders[j][i] == 1: # border of mapped area
                    information_gain = pygame.font.SysFont("monospace", 6)
                    inf_gain_text = "%.1f" % self.environment.agent.find_border(j, i)[3]
                    label = information_gain.render(inf_gain_text, 1, (0, 0, 0))
                    screen.blit(label, (x_pos, y_pos))

        # draw the agent on top of the grid
        agent_x = square_size * self.environment.agent.x + left_offset
        agent_y = square_size * self.environment.agent.y + top_offset
        pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(agent_x, agent_y, square_size-margin, square_size-margin), 0);

    def draw_outline(self):
        grid = self.environment.grid
        grid_width = len(grid[0])
        grid_height = len(grid)

        square_size = 0.9*800/max(grid_width, grid_height)
        top_offset = 10
        left_offset = 10

        # draw the grid
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                x_pos = square_size * j + left_offset
                y_pos = square_size * i + top_offset

                # pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(x_pos, y_pos, square_size, square_size), 1)


# load the grid
big_grid_file = open('sample_grid.txt', 'rb')
big_grid_lines = big_grid_file.read().splitlines()
big_grid = [[int(s) for s in line] for line in big_grid_lines]

big_grid_file.close()

environment = SlamEnvironment(big_grid)
environment.set_agent(5, 5)

pygame.init()
screen = pygame.display.set_mode((800, 800))
pygame.display.set_caption("The Curious Uncle SLAM")
screen.fill((0, 0, 0))

gui = SlamGuiTest(environment)

#rect1 = pygame.draw.rect(screen, (0, 128, 255), pygame.Rect(30, 30, 60, 60), 3)
done = False

# the game's clock
clock = pygame.time.Clock()

#clock speed. 0 means no fps limit
clock_speed = 0

while not done:
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        done = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        environment.on_left_key(event)
                    if event.key == pygame.K_RIGHT:
                        environment.on_right_key(event)
                    if event.key == pygame.K_UP:
                        environment.on_up_key(event)
                    if event.key == pygame.K_DOWN:
                        environment.on_down_key(event)
                    gui.draw_all()

        pygame.display.flip()
        clock.tick(clock_speed)

pygame.quit()