import math

class SlamAgent:
    def __init__(self, x, y):
        # x and y positions
        self.x = x
        self.y = y

        # line of sight radius
        self.line_of_sight = 4

        # the agent's environment
        self.environment = None

        # the agent's area in the environment that is mapped
        self.mapped_area = None
        self.mapped_walls = []

        # the mapped area's borders
        self.mapped_area_borders = None

        self.border_beliefs = []

        self.mapped_area_size = 0

    def in_line_of_sight(self, x, y):
        dist_x = self.x - x
        dist_y = self.y - y
        distance = math.sqrt(dist_x*dist_x + dist_y*dist_y)

        return distance <= self.line_of_sight

    # set the agent's environment and its limited perception
    def set_environment(self, environment):
        self.environment = environment
        grid = self.environment.grid

        # borders of the mapped area
        self.mapped_area_borders = []
        for i in range(len(grid)):
            self.mapped_area_borders.append([0]*len(grid[0]))

        # area that is mapped
        self.mapped_area = []
        for i in range(len(grid)):
            self.mapped_area.append([0]*len(grid[0]))

        self.update()

    def update(self):
        newly_mapped = []

        # update mapped area
        for i in range (self.y - self.line_of_sight, self.y + self.line_of_sight + 1):
            for j in range (self.x - self.line_of_sight, self.x + self.line_of_sight + 1):
                if self.environment.check_borders(j, i) and self.in_line_of_sight(j, i):
                    if not self.mapped_area[j][i] == 1:
                        newly_mapped.append((j, i)) # keep track of newly mapped cells
                        self.mapped_area_size += 1
                        if self.environment.grid[i][j] == 1: # keep track of mapped walls
                            self.mapped_walls.append((j, i))
                    self.mapped_area[j][i] = 1

        #update borders
        for coordinate in newly_mapped:
            j = coordinate[0]
            i = coordinate[1]

            for k in [i-1, i, i+1]:
                for l in [j-1, j, j+1]:
                    if not self.environment.check_borders(l, k):
                        continue

                    if self.mapped_area[l][k]:
                        # remove borders which are now mapped area
                        if self.mapped_area_borders[l][k] == 1:
                            self.remove_border(l, k)

                        self.mapped_area_borders[l][k] = 0
                    else:
                        # add new borders if not existing
                        if self.mapped_area_borders[l][k] == 0:
                            self.border_beliefs.append([l, k, 0.5, 0])

                        self.mapped_area_borders[l][k] = 1

        # update beliefs
        self.update_beliefs()

    def get_wall_probability_in_mapped_area(self):
        return float(len(self.mapped_walls))/float(self.mapped_area_size)

    def update_beliefs(self):
        for belief in self.border_beliefs:
            x = belief[0]
            y = belief[1]

            dist = math.sqrt((x-self.x)*(x-self.x) + (y-self.y)*(y-self.y))

            # only update nearby borders
            if dist > self.line_of_sight + 2:
                continue

            # belief[2] = 0.5 # reset prior to 0.5
            prior = belief[2]

            evidence_list = []

            for k in [y-3, y-2, y-1, y, y+1, y+2, y+3]:
                for l in [x-3, x-2, x-1, x, x+1, x+2, x+3]:
                    if not self.environment.check_borders(l, k) or not self.mapped_area[l][k] or (k == y and l == x):
                        continue
                    evidence_list.append(self.environment.grid[k][l])

            belief[2], belief[3] = self.calculate_posterior_max_likelihood_given_evidence(evidence_list)

    def remove_border(self, x, y):
        for border in self.border_beliefs:
            if border[0] == x and border[1] == y:
                self.border_beliefs.remove(border)

    def find_border(self, x, y):
        for border in self.border_beliefs:
            if border[0] == x and border[1] == y:
                return border

        return None

    def calculate_posterior_max_likelihood_given_evidence(self, evidence_list):
        res = 100

        biases = [(1.0/res)*i for i in range(1,res)]
        prior = [1.0/res for i in range(1,res)] # the prior array for bias belief

        posterior = prior[:] # init posterior as prior
        likelihood = [0] * res
        evidence_prob = [0] * res

        # get posterior
        for evidence in evidence_list:
            # get likelihood
            if evidence == 1:
                likelihood = [i[1] for i in enumerate(biases)]
            else: # evidence == 0
                likelihood = [1-i[1] for i in enumerate(biases)]

            # get evidence
            evidence_prob = sum([likelihood[i[0]]*posterior[i[0]] for i in enumerate(posterior)])

            # get posterior
            posterior = [likelihood[i[0]]*posterior[i[0]]/evidence_prob for i in enumerate(posterior)]

        information_gain = sum([prior[i[0]]*math.log(prior[i[0]]/posterior[i[0]]) for i in enumerate(prior)])
        max_a_posteriori = (1.0/res)*(posterior.index(max(posterior)) + 1)
        return max_a_posteriori, information_gain






