from SlamAgent import SlamAgent

class SlamEnvironment:
    """
    The SLAM simulator environment - represents all that happens within the simulator
    """

    # gets a 2D integer matrix
    def __init__(self, grid):
        self.grid = grid
        self.width = len(grid[0])
        self.height = len(grid)

    # set the agent on the environment
    def set_agent(self, x, y):
        self.agent = SlamAgent(x, y)
        self.agent.set_environment(self)

    def on_left_key(self, event):
        # move and update mapped area if valid move
        if self.check_movement(self.agent.x - 1, self.agent.y):
            self.agent.x -= 1
            self.agent.update()

    def on_right_key(self, event):
        # move and update mapped area if valid move
        if self.check_movement(self.agent.x + 1, self.agent.y):
            self.agent.x += 1
            self.agent.update()

    def on_up_key(self, event):
        # move and update mapped area if valid move
        if self.check_movement(self.agent.x, self.agent.y - 1):
            self.agent.y -= 1
            self.agent.update()

    def on_down_key(self, event):
        # move and update mapped area if valid move
        if self.check_movement(self.agent.x, self.agent.y + 1):
            self.agent.y += 1
            self.agent.update()

    def check_movement(self, new_x, new_y):
        # check borders
        if new_x < 0 or new_x >= self.width:
            return False
        if new_y < 0 or new_y >= self.height:
            return False

        # check for walls
        if self.grid[new_y][new_x] == 1:
            return False

        return True

    def check_borders(self, x, y):
        # check borders
        if x < 0 or x >= self.width:
            return False
        if y < 0 or y >= self.height:
            return False

        return True
